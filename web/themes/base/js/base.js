/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.cookies = {
        attach: function (context, settings) {
            $(document).ready(function() {
                $('.decline-button').on('click', function(){
                    $('#sliding-popup').remove();
                });
            });
        }
    };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.popUp = {
        attach: function (context, settings) {
            $(context).find('#block-popup').once('ifPopUp').each(function () {
                var cookiePopUpCheck = $.cookie("popUp-check");

                function checkCookie() {
                    cookiePopUpCheck = $.cookie("popUp-check", 1, {expires: 2});
                }
                if (!cookiePopUpCheck) {
                    $('#block-popup').foundation('open');
                }

                $('#close-pop-up').on('click', function () {
                    $("#block-popup").on("closed.zf.reveal", function (e) {
                        checkCookie();
                    });
                    $('#block-popup').foundation('close');
                });
            });
        }
    };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.gallery = {
        attach: function (context, settings) {
            $(context).find('.paragraph--type--gallery .field-name-field-images > .field-items > .field-item').once('ifGalleryItem').each(function (){
                var $grid = $('.paragraph--type--gallery .field-name-field-images > .field-items').masonry({
                    itemSelector: '.paragraph--type--gallery .field-name-field-images > .field-items > div',
                    columnWidth: '.paragraph--type--gallery .field-name-field-images > .field-items > div',
                    gutter: 20,
                    percentPosition: true,
                    horizontalOrder: true,
                });
                // layout Masonry after each image loads
                $grid.imagesLoaded().progress( function() {
                    $grid.masonry('layout');
                });
            });
        }
    };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-projects-featured.swiper-container').once('ifFeaturedProjects').each(function (){
                var featured_projects = new Swiper ('.field-name-field-projects-featured.swiper-container', {
                    autoplay: true,
                    loop: true,
                    effect: 'fade',
                    speed: 2000,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                });
            });
        }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasLeftOverlap').once('ifLeftCanvas').each(function () {
                $menuButton = $('#menuButton');
                $asideLeft = $('#left-aside');

                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                    $asideLeft.addClass('menu-open');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                    $asideLeft.removeClass('menu-open');
                });
            });
            $("#menuButton").click(function () {
                if ($("#block-social").hasClass('hidden')) {
                    $("#block-social").animate({
                        opacity: 1
                    }, 500, function () {
                        // Animation complete.
                    });
                    $("#block-social").removeClass('hidden');
                }
                else
                {
                    $("#block-social").animate({
                        opacity: 0
                    }, 500, function () {
                        // Animation complete.
                    });
                    $("#block-social").addClass('hidden');
                }
                
                if ($("#menuButton").hasClass('is-active')) {
                    $('.menu-text').html('menu');
                }
                else
                {
                    $('.menu-text').html('close');
                }
                
            });
            if ($(window).width() > 767 ) {
            $("#menuButton").click(function () { 
                $(".line_offcanvas").animate({
                    height: [850, "swing"] //easeinoutback
                }, 3500, function() {
                });
                });
            }
        }
    };

    Drupal.behaviors.scrollbar = {
        attach: function (context, settings) { 
             
            var initialTOP = $(".menu li a.is-active").offset().top - $(window).scrollTop() + 19;
            $(".squarescroll").css('top', initialTOP + 'px');
            $(".menu li a").mouseover(function () {
                var topx = $(this).offset().top;
                topOK = topx - $(window).scrollTop() + 19;
                $(".squarescroll").css('top', topOK + 'px');
             });
        }
    };




})(jQuery, Drupal);
